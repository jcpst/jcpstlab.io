About
=====

Short Version
-------------

I'm a composer and programmer in Kansas City.

My interests include analog synthesizers, Arch Linux, craft beer 
(IPAs), cycling, DevOps, erlang, functional programming, guitars, 
just intonation, my family, Node.js, tube amps, vegan cooking, wandering around, 
Zsh...

Slightly Longer Version
-----------------------

There were two pieces of electronics I really enjoyed as a child: a tape 
recorder that my mother bought me at a garage sale, and a portable computer 
running DOS.

When I was 11, a family friend flew into the US to buy an electric guitar. I
could not believe how cool that instrument was. Soon after, I picked up my first
bass guitar. Then formed bands, cut records, toured the US a few times, earned 
a B.M., wrote music that won awards and featured at various national conferences...

After college, I recorded music for seven years, everything from the Avett Bros 
to Gillie Da Kid ([credits](/credits)). I still produce albums for a few select
clients.

I was operating computers and all sorts of digital and analog electronics as an audio
engineer, and always craved deeper knowledge about how the hardware and software
I was using worked.

I got the programming bug, and these days I'm working on enterprise web apps, 
mostly in the realm of node/javascript and CI/Linux/DevOps stuff.

[Bandcamp fanpage](https://bandcamp.com/josephpost)

#### [photo](/assets/images/joefresco.jpg)

